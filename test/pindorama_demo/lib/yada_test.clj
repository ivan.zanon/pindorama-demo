(ns pindorama-demo.lib.yada-test
  (:require [midje.sweet :refer :all]
            [pindorama-demo.lib.db :as db]
            [pindorama-demo.lib.yada :as pdrm]
            [schema.core :as s]) 
  (:import [clojure.lang ExceptionInfo]
           [org.joda.time DateTime]))

(facts "about `default-search-response`"
       (fact "Call `db/search-entity` with conn from ctx"
             (pdrm/default-search-response ..schema.. {:config {:db-conn ..conn..}}) => []
             (provided (db/search-entity ..conn.. & anything) => []))

       (fact "Call `db/search-entity` with the schema"
             (pdrm/default-search-response ..schema.. ..ctx..) => []
             (provided (db/search-entity anything ..schema.. anything) => []))

       (fact "Call `db/search-entity` with the query from parameters"
             (pdrm/default-search-response ..schema.. {:parameters {:query ..query..}}) => []
             (provided (db/search-entity anything anything ..query..) => []))

       (fact "Returns the result of `db/search-entity`"
             (pdrm/default-search-response & anything) => [{:id ..id1..}
                                                           {:id ..id2..}]
             (provided (db/search-entity & anything) => [{:id ..id1..}
                                                         {:id ..id2..}])))

(facts "about `default-create-response`"
       (fact "calls `db/create-entity` with the conn from ctx"
             (pdrm/default-create-response ..schema.. {:config {:db-conn ..conn..}}) => {}
             (provided (db/create-entity ..conn.. & anything) => {}))

       (fact "calls `db/create-entity` with the schema"
             (pdrm/default-create-response ..schema.. ..ctx..) => {}
             (provided (db/create-entity anything ..schema.. anything) => {}))

       (fact "calls `db/create-entity` with the body with id"
             (pdrm/default-create-response ..schema.. {:parameters {:body {:any :thing}}}) => {}
             (provided (db/create-entity anything anything (contains {:any :thing
                                                                      :id string?})) => {})))

(facts "about `default-search-by-id-response`"
       (fact "Call `db/search-entity` with conn from ctx"
             (pdrm/default-search-by-id-response ..schema.. {:config {:db-conn ..conn..}}) => {}
             (provided (db/search-entity ..conn.. & anything) => [{}]))

       (fact "Call `db/search-entity` with the schema"
             (pdrm/default-search-by-id-response ..schema.. ..ctx..) => {}
             (provided (db/search-entity anything ..schema.. anything) => [{}]))

       (fact "Call `db/search-entity` with path"
             (pdrm/default-search-by-id-response ..schema.. {:parameters {:path ..path..}}) => {}
             (provided (db/search-entity anything anything ..path..) => [{}]))

       (fact "Returns the first result of `db/search-entity`"
             (pdrm/default-search-by-id-response & anything) => {:id ..id..}
             (provided (db/search-entity & anything) => [{:id ..id..}])))

(facts "about `default-delete-response`"
       (fact "Call `db/delete-entity` with conn from ctx"
             (pdrm/default-delete-response ..schema.. {:config {:db-conn ..conn..}}) => anything
             (provided (db/delete-entity ..conn.. & anything) => 1))

       (fact "Call `db/delete-entity` with the schema"
             (pdrm/default-delete-response ..schema.. ..ctx..) => anything
             (provided (db/delete-entity anything ..schema.. anything) => 1))

       (fact "Call `db/delete-entity` with id from path"
             (pdrm/default-delete-response ..schema.. {:parameters {:path {:id ..id..}}}) => anything
             (provided (db/delete-entity anything anything ..id..) => 1))

       (fact "Returns success message if `db/delete-entity` returns 1"
             (pdrm/default-delete-response ..schema.. {:parameters {:path {:id ..id..}}}) => {:message "Entity [..id..] deleted."}
             (provided (db/delete-entity anything anything ..id..) => 1))

       (fact "Throws if `db/delete-entity` returns 0"
             (pdrm/default-delete-response ..schema.. {:parameters {:path {:id ..id..}}}) => (throws ExceptionInfo #"Nothing deleted")
             (provided (db/delete-entity anything anything ..id..) => 0)))

(facts "about `default-update-response`"
       (fact "Call `db/update-entity` with conn from ctx"
             (pdrm/default-update-response ..schema.. {:config {:db-conn ..conn..}}) => anything
             (provided (db/update-entity ..conn.. & anything) => {}))

       (fact "Call `db/update-entity` with the schema"
             (pdrm/default-update-response ..schema.. ..ctx..) => anything
             (provided (db/update-entity anything ..schema.. & anything) => {}))

       (fact "Call `db/update-entity` with id from path"
             (pdrm/default-update-response ..schema.. {:parameters {:path {:id ..id..}}}) => anything
             (provided (db/update-entity anything anything ..id.. anything) => {}))
       
       (fact "Call `db/update-entity` with id from path"
             (pdrm/default-update-response ..schema.. {:parameters {:body ..body..}}) => anything
             (provided (db/update-entity anything anything anything ..body..) => {}))
       
       (fact "Returns the result of `db/update-entity`"
             (pdrm/default-update-response ..schema.. ..ctx..) => {:id ..id..}
             (provided (db/update-entity & anything) => {:id ..id..})))

(s/defschema BaseEntity {:id s/Uuid
                         :name s/Str
                         :number s/Num
                        ;;  :date DateTime TODO!
                         })
(s/defschema FilterEntity {(s/optional-key :id) s/Uuid
                           (s/optional-key :name) s/Str
                           (s/optional-key :number) s/Num
                           (s/optional-key :paginationPage) s/Num
                           (s/optional-key :paginationLimit) s/Num
                           (s/optional-key :orderByColumns) [s/Str]
                           (s/optional-key :orderByDesc) s/Bool
                          ;;  (s/optional-key :date-from) DateTime
                          ;;  (s/optional-key :date-to) DateTime
                           })
(s/defschema CreateEntity {:name s/Str
                           :number s/Num
                          ;;  :date DateTime
                           })

(facts "about `generate-search`"
       (fact "generate the config based on schema"
             (pdrm/generate-search BaseEntity) => (just {:parameters {:query FilterEntity}
                                                         :swagger/summary "Search baseentity using filters."
                                                         :swagger/responses {200 {:schema [BaseEntity]}}
                                                         :response fn?}))

       (fact "accepts and apply custom properties"
             (pdrm/generate-search BaseEntity
                                   {:other :stuff
                                    :swagger/summary "Custom summary"}) => (just {:parameters {:query FilterEntity}
                                                                                  :swagger/summary "Custom summary"
                                                                                  :swagger/responses {200 {:schema [BaseEntity]}}
                                                                                  :response fn?
                                                                                  :other :stuff})))

(facts "about `generate-create"
       (fact "generate the config based on schema"
             (pdrm/generate-create BaseEntity) => (just {:consumes "application/json"
                                                         :parameters {:body CreateEntity}
                                                         :swagger/summary "Creates baseentity"
                                                         :swagger/responses {200 {:schema BaseEntity}}
                                                         :response fn?}))

       (fact "accept and apply custom properties"
             (pdrm/generate-create BaseEntity
                                   {:other :stuff
                                    :swagger/summary "Custom summary"}) => (just {:consumes "application/json"
                                                                                  :parameters {:body CreateEntity}
                                                                                  :swagger/summary "Custom summary"
                                                                                  :swagger/responses {200 {:schema BaseEntity}}
                                                                                  :response fn?
                                                                                  :other :stuff})))

(facts "about `generate-get`"
       (fact "generate the config based on schema"
             (pdrm/generate-get BaseEntity) => (just {:parameters {:path {:id s/Uuid}}
                                                      :swagger/summary "Get baseentity by ID."
                                                      :swagger/responses {200 {:schema BaseEntity}}
                                                      :response fn?}))
       
       (fact "accept and apply custom properties"
             (pdrm/generate-get BaseEntity
                                {:other :stuff
                                 :swagger/summary "Custom summary"}) => (just {:parameters {:path {:id s/Uuid}}
                                                                               :swagger/summary "Custom summary"
                                                                               :swagger/responses {200 {:schema BaseEntity}}
                                                                               :response fn?
                                                                               :other :stuff})))

(facts "about `generate-delete`"
       (fact "generate the config based on schema"
             (pdrm/generate-delete BaseEntity) => (just {:parameters {:path {:id s/Uuid}}
                                                         :swagger/summary "Deletes baseentity by ID."
                                                         :swagger/responses {200 {:schema {:message s/Str}}}
                                                         :response fn?}))

       (fact "accept and apply custom properties"
             (pdrm/generate-delete BaseEntity
                                   {:other :stuff
                                    :swagger/summary "Custom summary"}) => (just {:parameters {:path {:id s/Uuid}}
                                                                               :swagger/summary "Custom summary"
                                                                               :swagger/responses {200 {:schema {:message s/Str}}}
                                                                               :response fn?
                                                                               :other :stuff})))

(facts "about `generate-update`"
       (fact "generate the config based on schema"
             (pdrm/generate-update BaseEntity) => (just {:consumes "application/json"
                                                         :parameters {:path {:id s/Uuid}
                                                                      :body CreateEntity}
                                                         :swagger/summary "Replace baseentity by ID."
                                                         :swagger/responses {200 {:schema BaseEntity}}
                                                         :response fn?}))

       (fact "accept and apply custom properties"
             (pdrm/generate-update BaseEntity
                                   {:other :stuff
                                    :swagger/summary "Custom summary"}) => (just {:consumes "application/json"
                                                                                  :parameters {:path {:id s/Uuid}
                                                                                               :body CreateEntity}
                                                                                  :swagger/summary "Custom summary"
                                                                                  :swagger/responses {200 {:schema BaseEntity}}
                                                                                  :response fn?
                                                                                  :other :stuff})))