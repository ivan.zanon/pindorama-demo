(ns pindorama-demo.lib.db-test
  (:require [midje.sweet :refer :all]
            [pindorama-demo.lib.db :as db]
            [schema.core :as s]
            [next.jdbc :as jdbc]
            [java-time :as jt])
  (:import [org.joda.time DateTime]))

(s/defschema Entity 
  {:id s/Uuid
   :name s/Str
   :number s/Num
   :date DateTime})

(facts "about `search-entity`"
       (fact "select all if no filter provided"
             (db/search-entity ..conn.. Entity {}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity LIMIT ? OFFSET ?" 100 0] anything) => []))

       (fact "filter numbers with ="
             (db/search-entity ..conn.. Entity {:number 2}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity WHERE number = ? LIMIT ? OFFSET ?" 2 100 0] anything) => []))

       (fact "filter uuid with ="
             (db/search-entity ..conn.. Entity {:id ..uuid..}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity WHERE id = ? LIMIT ? OFFSET ?" ..uuid.. 100 0] anything) => []))

       (fact "filter strings with LIKE"
             (db/search-entity ..conn.. Entity {:name "abc"}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity WHERE name LIKE ? LIMIT ? OFFSET ?" "%abc%" 100 0] anything) => []))

       (fact "Use default values for limit and offset if not set"
             (db/search-entity ..conn.. Entity {}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity LIMIT ? OFFSET ?" 100 0] anything) => []))
       
       (fact "Use the limit and page from filter"
             (db/search-entity ..conn.. Entity {:pagination-page 10 :pagination-limit 10}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity LIMIT ? OFFSET ?" 10 100] anything) => []))
       
       (fact "Use order by"
             (db/search-entity ..conn.. Entity {:order-by-columns ["column1" "column2"]
                                                :order-by-desc true}) => []
             (provided (jdbc/execute! ..conn.. ["SELECT * FROM entity ORDER BY column1 DESC, column2 DESC LIMIT ? OFFSET ?" 100 0] anything) => []))
       
       (future-facts "test the DateTime period"))

(facts "about `create-entity`"
       (fact "insert the entity with all columns to the correct table"
             (db/create-entity ..conn.. Entity {:id ..uuid..
                                                :name ..name..
                                                :number ..number..
                                                :date ..date..}) => {:id ..uuid..
                                                                     :name ..name..
                                                                     :number ..number..
                                                                     :date ..date..}
             (provided (jdbc/execute-one! ..conn..
                                          ["INSERT INTO entity (id, name, number, date) VALUES (?, ?, ?, ?)"
                                           ..uuid.. ..name.. ..number.. ..date..] anything) => {}))

       (fact "It converts DateTime to LocalDateTime"
             (let [date-time (DateTime/parse "2022-02-02T00:00:00.000Z")]
               (db/create-entity ..conn.. Entity {:id ..uuid..
                                                  :name ..name..
                                                  :number ..number..
                                                  :date date-time}) => {:id ..uuid..
                                                                        :name ..name..
                                                                        :number ..number..
                                                                        :date date-time}
               (provided (jdbc/execute-one! ..conn..
                                            ["INSERT INTO entity (id, name, number, date) VALUES (?, ?, ?, ?)"
                                             ..uuid.. ..name.. ..number.. (jt/local-date-time date-time)] anything) => {}))))

(facts "about `update-entity`"
       (fact "insert the entity with all columns to the correct table"
             (db/update-entity ..conn.. Entity ..uuid..
                               {:name ..name..
                                :number ..number..
                                :date ..date..}) => {:id ..uuid..
                                                     :name ..name..
                                                     :number ..number..
                                                     :date ..date..}
             (provided (jdbc/execute-one! ..conn..
                                          ["UPDATE entity SET name = ?, number = ?, date = ? WHERE id = ?"
                                           ..name.. ..number.. ..date.. "..uuid.."] anything) => {}))

       (fact "It converts DateTime to LocalDateTime"
             (let [date-time (DateTime/parse "2022-02-02T00:00:00.000Z")]
               (db/update-entity ..conn.. Entity ..uuid..
                                 {:name ..name..
                                  :number ..number..
                                  :date date-time}) => {:id ..uuid..
                                                        :name ..name..
                                                        :number ..number..
                                                        :date date-time}
               (provided (jdbc/execute-one! ..conn..
                                            ["UPDATE entity SET name = ?, number = ?, date = ? WHERE id = ?" 
                                             ..name.. ..number.. (jt/local-date-time date-time) "..uuid.."] anything) => {}))))



(facts "about `delete-entity`"
       (fact "delete the entity with the correct id"
             (db/delete-entity ..conn.. Entity ..uuid..) => 1
             (provided (jdbc/execute-one! ..conn..
                                          ["DELETE FROM entity WHERE id = ?" 
                                           "..uuid.."] anything) => {:next.jdbc/update-count 1})))