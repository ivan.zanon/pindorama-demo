(defproject pindorama-demo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/tools.logging "1.2.4"]
                 [ch.qos.logback/logback-core "1.4.5"]
                 [ch.qos.logback/logback-classic "1.4.5"]
                 [yada "1.4.0-alpha1"]
                 [aleph "0.4.7"]
                 [bidi "2.1.6"]
                 [com.github.seancorfield/honeysql "2.4.947"]
                 [com.github.seancorfield/next.jdbc "1.3.847"]
                 [com.zaxxer/HikariCP "5.0.1"]
                 [clojure.java-time "1.1.0"]
                 [mysql/mysql-connector-java "8.0.31"]
                 [yogthos/config "1.2.0"]
                 [camel-snake-kebab "0.4.3"]]
  :main ^:skip-aot pindorama-demo.core
  :target-path "target/%s"
  :profiles {:dev {:dependencies [[midje "1.10.9"]]
                   :plugins [[lein-midje "3.2.2"]
                             [lein-cloverage "1.2.4"]]}
             :uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
