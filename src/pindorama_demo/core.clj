(ns pindorama-demo.core
  (:gen-class)
  (:require [clojure.tools.logging :as log]
            [config.core :refer [env]]
            [java-time :as jt]
            [next.jdbc.connection :as conn]
            [pindorama-demo.lib.common :refer [filter-map-by-prefix]]
            [pindorama-demo.lib.db :as db]
            [pindorama-demo.lib.yada :as pdrm]
            [schema.core :as s]
            [yada.handler :as handler]
            [yada.yada :as yada]
            [yada.walk :as yw])
  (:import [com.zaxxer.hikari HikariDataSource]))

(def DateTime org.joda.time.DateTime)

(defonce db-conn
  (conn/->pool HikariDataSource
               (filter-map-by-prefix :pindorama-db- env)))

(defn ctx->db-conn [ctx]
  (get-in ctx [:config :db-conn])) ;;TODO Ask for a function that specify where (in the ctx) the db connection is.

(s/defschema Project
  {:id s/Uuid
   :number s/Num
   :name s/Str
   :description s/Str
   :startDate DateTime
   })

(s/defschema Customer
  {:id s/Uuid
   :name s/Str
   :address s/Str
   :vip s/Bool
   :lastName s/Str
  ;;  :startDate DateTime
   })

(def number-regex #"^[0-9]*$")

(defn search-project [ctx]
  (->> (db/search-entity (ctx->db-conn ctx) Project (get-in ctx [:parameters :query]))
       (map #(update % :start-date str)) ;;TODO configurar o encode JSON para LocalDateTime 
       ))

(defn body-in [body]
  (-> body
      (assoc :start-date (:startDate body))
      (dissoc :startDate)
      (update :start-date jt/local-date-time))) ;; TODO translate otherNames to other-names
                                                ;; and also all dates to LocalDateTime

(defn create-project [ctx]
  (let [body (-> (body-in (get-in ctx [:parameters :body]))
                 (assoc :id (str (java.util.UUID/randomUUID))))] 
    (-> (db/create-entity (ctx->db-conn ctx) Project body)
        (update :start-date str) ;;TODO configurar o encode JSON para LocalDateTime 
        )))

(defn get-project-by-id [ctx]
   (-> (db/search-entity (ctx->db-conn ctx) Project (get-in ctx [:parameters :path]))
       first
       (update :start-date str) ;;TODO configurar o encode JSON para LocalDateTime 
       ))

(defn delete-project [ctx]
  (let [id (get-in ctx [:parameters :path :id])
        result (db/delete-entity (ctx->db-conn ctx) Project id)]
    (if (> (:next.jdbc/update-count result) 0)
      {:message (str "Entity [" id "] deleted.")}
      (throw (ex-info "Nothing deleted."
                      {:status 404
                       :id (str id)})))))

(defn update-project [ctx]
  (let [body (body-in (get-in ctx [:parameters :body]))]
    (-> (db/update-entity (ctx->db-conn ctx) Project (get-in ctx [:parameters :path :id]) body)
        (update :start-date str) ;;TODO configurar o encode JSON para LocalDateTime 
        )))

(defn- interceptor-add-db-conn [ctx]
  (log/debug "Intercepting context: Adding db-conn.")
  (assoc-in ctx [:config :db-conn] db-conn))

(defonce server (atom nil))

(defn start-server []
  (swap! server (fn [x]
                  (when x
                    (log/info "Closing existing server")
                    ((:close x)))
                  (log/info "Starging new server!")
                  (yada/listener
                   ["/api" (-> ["/"
                                [["test" (yada/resource {:produces "text/plain"
                                                         :response "This is a test!"
                                                         :swagger/tags "test"})]
                                 (-> ["custom_project"
                                      [["" (-> {:produces "application/json"
                                                :swagger/tags "CustomProject"
                                                :methods {:get (pdrm/generate-search Project)
                                                          :post (pdrm/generate-create Project)}}
                                               (assoc :interceptor-chain yada.yada/default-interceptor-chain)
                                               (yada/resource))]
                                       [["/" :id] (yada/resource {:produces "application/json"
                                                                  :interceptor-chain yada.yada/default-interceptor-chain
                                                                  :swagger/tags "CustomProject"
                                                                  :methods {:get (pdrm/generate-get Project)
                                                                            :delete (pdrm/generate-delete Project)
                                                                            :put (pdrm/generate-update Project)}})]]]
                                     (pdrm/add-converter-interceptors))
                                 (pdrm/generate-resource Project)
                                 (pdrm/generate-resource Customer)
                                 ]]
                               (yw/update-routes handler/prepend-interceptor interceptor-add-db-conn)
                               (yada/swaggered
                                {:info {:title "Pindorama Demo API"
                                        :description "REST api to receive Pindorama lib"}
                                 :basePath "/api"}))]
                   {:port 3000
                    :config {:db-conn db-conn}}))))