(ns pindorama-demo.lib.yada 
  (:require [camel-snake-kebab.core :as csk]
            [clojure.tools.logging :as log]
            [clojure.walk :as walk]
            [pindorama-demo.lib.common :as common]
            [pindorama-demo.lib.db :as db]
            [schema.core :as s]
            [yada.handler :as handler]
            [yada.interceptors :as yi]
            [yada.walk :as yw]
            [yada.yada :as yada]) 
  (:import [org.joda.time LocalDateTime]))

(defn- convert-struct
  "Converts given structure by applying `f` to all map keys."
  [f s]
  (let [conv (fn [x]
               (if (map-entry? x)
                 [(f (first x)) (second x)]
                 x))]
    (walk/prewalk conv s)))

(defn- kebab-case-struct [x]
  (convert-struct csk/->kebab-case-keyword x))

(defn- camelCaseStruct [x]
  (log/debug "Intercepting context: Converting response - Kebab -> Camel.")
  (convert-struct csk/->camelCaseKeyword x))

(defn- interceptor-request-converter
  "Takes the request body and converts it to kebab-case"
  [ctx]
  (log/debug "Intercepting context: Converting request - Camel -> Kebab.")
  (log/debug "Converting request: " (:parameters ctx))
  (-> ctx
      (update-in [:parameters :body] kebab-case-struct)
      (update-in [:parameters :query] kebab-case-struct)))

(defn- interceptor-response-converter
  "Convertes the response body keys from kebab-case to camelCase"
  [ctx]
  (log/debug "Converting response:" (get-in ctx [:response :body]))
  (update-in ctx [:response :body] camelCaseStruct))

(defn add-converter-interceptors [routes]
  (-> routes
      (yw/update-routes handler/append-interceptor yi/process-request-body interceptor-request-converter)
      (yw/update-routes handler/insert-interceptor yi/create-response interceptor-response-converter)))

(defn- ctx->db-conn [ctx]
  (get-in ctx [:config :db-conn])) ;;TODO Ask for a function that specify where (in the ctx) the db connection is.

(defn- schema->post-schema 
  "Removes the id of the schema, that should not be present on POST requests"
  [schema]
  (dissoc schema :id))

(defn- schema->filter-schema [schema]
  (as-> (for [[k v] schema] {(s/optional-key k) v}) filter
    (into {} filter)
    (assoc filter
           (s/optional-key :paginationPage) s/Num
           (s/optional-key :paginationLimit) s/Num
           (s/optional-key :orderByColumns) [s/Str]
           (s/optional-key :orderByDesc) s/Bool)))

(defn default-search-response [schema ctx]
  (db/search-entity (ctx->db-conn ctx) schema (get-in ctx [:parameters :query])))

(defn default-create-response [schema ctx]
  (let [body (-> (get-in ctx [:parameters :body])
                 (assoc :id (str (java.util.UUID/randomUUID))))]
    (db/create-entity (ctx->db-conn ctx) schema body)))

(defn default-search-by-id-response [schema ctx]
  (-> (db/search-entity (ctx->db-conn ctx) schema (get-in ctx [:parameters :path]))
      first))

(defn default-delete-response [schema ctx]
  (let [id (get-in ctx [:parameters :path :id])
        result (db/delete-entity (ctx->db-conn ctx) schema id)]
    (if (= result 1)
      {:message (str "Entity [" id "] deleted.")}
      (throw (ex-info "Nothing deleted."
                      {:status 404
                       :id (str id)})))))

(defn default-update-response [schema ctx]
  (let [body (get-in ctx [:parameters :body])
        id (get-in ctx [:parameters :path :id])]
    (db/update-entity (ctx->db-conn ctx) schema id body)))

(defn generate-create [schema & [config]]
  (merge {:parameters {:body (schema->post-schema schema)}
          :consumes "application/json"
          :swagger/summary (str "Creates " (common/meta-name schema))
          :swagger/responses {200 {:schema schema}}
          :response (partial default-create-response schema)}
         config))

(defn generate-search [schema & [config]]
  (merge {:parameters {:query (schema->filter-schema schema)}
          :swagger/summary (str "Search " (common/meta-name schema) " using filters.")
          :swagger/responses {200 {:schema [schema]}}
          :response (partial default-search-response schema)}
         config))

(defn generate-get [schema & [config]]
  (merge {:parameters {:path {:id s/Uuid}}
          :swagger/summary (str "Get " (common/meta-name schema) " by ID.")
          :swagger/responses {200 {:schema schema}}
          :response (partial default-search-by-id-response schema)}
         config))

(defn generate-delete [schema & [config]]
  (merge {:parameters {:path {:id s/Uuid}}
          :swagger/summary (str "Deletes " (common/meta-name schema) " by ID.")
          :swagger/responses {200 {:schema {:message s/Str}}}
          :response (partial default-delete-response schema)}
         config))

(defn generate-update [schema & [config]]
  (merge {:parameters {:path {:id s/Uuid}
                       :body (schema->post-schema schema)}
          :consumes "application/json"
          :swagger/summary (str "Replace " (common/meta-name schema) " by ID.")
          :swagger/responses {200 {:schema schema}}
          :response (partial default-update-response schema)}
         config))

(defn generate-resource [schema]
  (->  [(common/meta-name schema)
        [["" (-> {:produces "application/json"
                  :methods {:get (generate-search schema)
                            :post (generate-create schema)}}
                 (assoc :interceptor-chain yada/default-interceptor-chain)
                 (yada/resource))]
         [["/" :id] (-> {:produces "application/json"
                         :methods {:get (generate-get schema)
                                   :delete (generate-delete schema)
                                   :put (generate-update schema)}}
                        (assoc :interceptor-chain yada/default-interceptor-chain)
                        (yada/resource))]]]
       (add-converter-interceptors)))