(ns pindorama-demo.lib.db
  (:require [clojure.tools.logging :as log]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]
            [next.jdbc :as jdbc]
            [next.jdbc
             [result-set :as rs]]
            [pindorama-demo.lib.common :refer [meta-name-keyword]]
            [schema.core :as s]
            [java-time :as jt]) 
  (:import [org.joda.time DateTime]))

(defn with-default-options
  ([options]
   (assoc options :builder-fn rs/as-unqualified-kebab-maps))
  ([]
   (with-default-options {})))

(def default-options (with-default-options))

#_(defn execute-query [schema]
  (jdbc/execute-one!
   db-conn
   [(str "Select * from " (meta-name schema))]
   (with-default-options)))

(defn convert-date-time [map]
  (->> (for [[k v] map]
         [k (condp = (type v)
              java.time.LocalDateTime (str v)
              DateTime (jt/local-date-time v)
              v)])
       (into {})))

(defn search-where [query [key value]]
  (let [value-type (type value)]
    (condp = value-type
      s/Str (h/where query [:like key (str "%" value "%")])
      s/Uuid (h/where query [:= key (str value)])
      DateTime (h/where query [:= key (str value)])
      (h/where query [:= key value]))))

(defn search-limit [query filter]
  (let [limit (get filter :pagination-limit 100)
        page (get filter :pagination-page 0)]
    (-> query
        (h/limit limit)
        (h/offset (* page limit)))))

(defn- add-order-by [desc query column]
  (h/order-by query [column (if desc :desc :asc)]))

(defn- search-order-by [query {:keys [order-by-columns order-by-desc]}]
   (let [columns (map keyword order-by-columns)]
    (reduce (partial add-order-by order-by-desc) query columns )))

(defn search-entity [conn schema filter]
  (let [where-paremeters (dissoc filter :pagination-page :pagination-limit :order-by-columns :order-by-desc)
        where (->>  (for [[k v] where-paremeters] [k v])
                    (reduce search-where {}))
        query (-> where
                  (h/select :*)
                  (h/from (meta-name-keyword schema))
                  (search-limit filter)
                  (search-order-by filter)
                  (sql/format))]
    (log/info "Executing query:" query)
    (->> (jdbc/execute! conn query default-options)
         (map convert-date-time))))

(defn create-entity [conn schema body]
  (let [query (-> (h/insert-into (meta-name-keyword schema))
                  (h/values [(convert-date-time body)])
                  (sql/format))]
    (log/info "Executing query:" query)
    (jdbc/execute-one! conn query default-options)
    body))

(defn update-entity [conn schema id body]
  (let [query (-> (h/update (meta-name-keyword schema))
                  (h/set (convert-date-time body))
                  (h/where [:= :id (str id)])
                  (sql/format))]
    (log/info "Executing query:" query)
    (jdbc/execute-one! conn query default-options)
    (assoc body :id id)))

(defn delete-entity [conn schema id]
  (let [query (-> (h/delete-from (meta-name-keyword schema))
                  (h/where [:= :id (str id)])
                  (sql/format))]
    (log/info "Executing query:" query)
    (-> (jdbc/execute-one! conn query default-options)
        (:next.jdbc/update-count))))