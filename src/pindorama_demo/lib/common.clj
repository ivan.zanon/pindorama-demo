(ns pindorama-demo.lib.common 
  (:require [clojure.string :as string]))

(defn meta-name [schema]
  (string/lower-case (:name (meta schema))))

(defn meta-name-keyword [schema]
  (keyword (meta-name schema)))

(defn- prefix-filter [prefix]
  (let [n (name prefix)]
    (fn [[k _]]
      (.startsWith (name k) n))))

(defn- drop-prefix [prefix]
  (let [l (count (name prefix))]
    (fn [[k v]]
      [(keyword (subs (name k) l)) v])))

(defn filter-map-by-prefix [prefix m]
  (->> m
       (filter (prefix-filter prefix))
       (map (drop-prefix prefix))
       (into {})))