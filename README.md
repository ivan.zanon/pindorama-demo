# pindorama-demo

A demo project that aims on creating a lib that helps to generate yada CRUD endpoints.

- Yet to be implemented
	- [ ] Transform dates on `filter` to periods
	- [ ] Improve `DateTime` conversion (is working with `str`)
	- [ ] Migrations
	- [ ] add orderby clause on search
	- [ ] (maybe) Specify columns to the search methds
	- [ ] (maybe) Customize configurations in resource level
	- [ ] scan database to infere schemas
	- [ ] configure database or migration as priority fr the lib.
	- [ ] take a look at (REITIT)[https://github.com/metosin/reitit]
	- [x] Use `log` instead of prints
	- [x] Add pagination to search
	- [x] Write tests for `db`

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar pindorama-demo-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2023 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
